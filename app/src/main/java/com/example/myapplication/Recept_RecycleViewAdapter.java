package com.example.myapplication;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Objects;

public class Recept_RecycleViewAdapter extends RecyclerView.Adapter<Recept_RecycleViewAdapter.MyViewHolder> {
    private final RecycleViewInteface recycleViewInteface;
    Context context;
    ArrayList<ReceptModel> receptModels;
    public Recept_RecycleViewAdapter(Context context, ArrayList<ReceptModel> receptModels, RecycleViewInteface recycleViewInteface) {
        this.context = context;
        this.receptModels = receptModels;
        this.recycleViewInteface = recycleViewInteface;

    }



    @NonNull
    @Override
    public Recept_RecycleViewAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.recycle_view_row, parent, false);
        return new Recept_RecycleViewAdapter.MyViewHolder(view, recycleViewInteface);
    }

    @Override
    public void onBindViewHolder(@NonNull Recept_RecycleViewAdapter.MyViewHolder holder, int position) {
        holder.tvName.setText(receptModels.get(position).getReceptName());
        holder.tv3betu.setText(receptModels.get(position).getReceptAbbrevation());
        holder.tv1betu.setText(receptModels.get(position).getReceptAbbrevationSmall());
        holder.imageView.setImageResource(receptModels.get(position).getImage());
    }

    @Override
    public int getItemCount() {
        return receptModels.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        ImageView imageView;
        TextView tvName, tv3betu, tv1betu;
        public MyViewHolder(@NonNull View itemView, RecycleViewInteface recycleViewInteface) {
            super(itemView);

            imageView = itemView.findViewById(R.id.imageView);
            tvName = itemView.findViewById(R.id.textView);
            tv3betu = itemView.findViewById(R.id.textView2);
            tv1betu = itemView.findViewById(R.id.textView3);
            Objects.requireNonNull(itemView).setOnClickListener( new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (recycleViewInteface != null) {
                        int pos = getAdapterPosition();

                        if (pos != RecyclerView.NO_POSITION) {
                            recycleViewInteface.onItemClick(pos);
                        }

                    }
                }
            });

        }
    }
}




