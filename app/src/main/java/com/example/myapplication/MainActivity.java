package com.example.myapplication;

import android.content.Intent;
import android.os.Bundle;

import androidx.activity.EdgeToEdge;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements RecycleViewInteface {

    ArrayList <ReceptModel> receptModels = new ArrayList<>();

    int[] receptImages= {R.drawable.bolognai, R.drawable.borso_leves,R.drawable.brassoi,R.drawable.chilis_bab,
            R.drawable.csiramale,R.drawable.hamburger,R.drawable.krumplis_teszta,R.drawable.paprikas_krumpli,
            R.drawable.pizza, R.drawable.rantott_hus};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        RecyclerView recyclerView = findViewById(R.id.rRecycleView);

        setUpReceptModels();

        Recept_RecycleViewAdapter adapter = new Recept_RecycleViewAdapter(this, receptModels, this );
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    };

    private void setUpReceptModels() {

        String []  receptName = getResources().getStringArray(R.array.recept_txt);
        String []  receptAbbrevation = getResources().getStringArray(R.array.recept_harom_betu_txt);
        String []  receptAbbrevationSmall = getResources().getStringArray(R.array.recept_egy_betu_txt);
        String []  description = getResources().getStringArray(R.array.description_txt);

        for (int i = 0; i<receptName.length; i++){

            receptModels.add(new ReceptModel(receptName[i],
                    receptAbbrevation[i],
                    receptAbbrevationSmall[i],
                    receptImages[i],
                    description[i]
            ));
        }

    }

    @Override
    public void onItemClick(int position) {

        Intent intent = new Intent(MainActivity.this, MainActivity2.class);
        intent.putExtra("NÉV", receptModels.get(position).getReceptName());
        intent.putExtra("ABBR_BIG", receptModels.get(position).getReceptAbbrevation());
        intent.putExtra("ABBR_SMALL", receptModels.get(position).getReceptAbbrevationSmall());
        intent.putExtra("DESCRIPTION", receptModels.get(position).getDescription());
        intent.putExtra("IMAGE",receptModels.get(position).getImage());

        startActivity (intent);

    }
}
