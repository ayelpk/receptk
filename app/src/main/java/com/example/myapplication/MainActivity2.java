package com.example.myapplication;


import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

public class MainActivity2 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        String name = getIntent().getStringExtra("NÉV");
        String abbrbig = getIntent().getStringExtra("ABBR_BIG");
        String abbrsmall = getIntent().getStringExtra("ABBR_SMALL");
        String description = getIntent().getStringExtra("DESCRIPTION");
        int image = getIntent().getIntExtra("IMAGE", 0);

        TextView nameTextView = findViewById(R.id.recept);
        TextView receptharombetuTextView = findViewById(R.id.receptharombetu);
        TextView receptegybetuTextView = findViewById(R.id.receptegybetu);
        TextView descriptionTextView = findViewById(R.id.description);
        ImageView Image = findViewById(R.id.imageView2);

        nameTextView.setText(name);
        receptharombetuTextView.setText(abbrbig);
        receptegybetuTextView.setText(abbrsmall);
        descriptionTextView.setText(description);
        Image.setImageResource(image);






    }
}