package com.example.myapplication;

public class ReceptModel {
    String receptName;
    String receptAbbrevation;
    String receptAbbrevationSmall;
    String description;
    int image;

    public ReceptModel(String receptName, String receptAbbrevation, String receptAbbrevationSmall, int image, String description) {
        this.receptName = receptName;
        this.receptAbbrevation = receptAbbrevation;
        this.receptAbbrevationSmall = receptAbbrevationSmall;
        this.description = description;
        this.image = image;
    }

    public String getReceptName() {
        return receptName;
    }

    public String getReceptAbbrevation() {
        return receptAbbrevation;
    }

    public String getReceptAbbrevationSmall() {
        return receptAbbrevationSmall;
    }
    public String getDescription() {
        return description;
    }

    public int getImage() {
        return image;
    }







}
